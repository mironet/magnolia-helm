# `enabled` flag combinations

The magnolia-helm chart (currently) contains 36 different `enabled` flags. This
allows for a very large number of possible combinations (68'719'476'736). This
complexity is hard to handle when it comes to the testing of the chart. It is
not possible to test all possible combinations of flags. Thus, we have to settle
for a few selected combinations that are tested. This document is intended to
help find these selected combinations for which we should do the testing.

This document is also intended to facilitate, for the helm chart user, the
process of finding and setting the correct combination of `enabled` flags for a
given use case.

## Common use cases

### Restore

To restore from a backup bundle (using `db.restore.bundle_url`), the following
flags must be enabled:

- `db`
- `db.restore`
- `db.contentsync`

Note that `db.backup` must not necessarily be enabled for the restore to work.

> Respective [helm unittests](https://github.com/helm-unittest/helm-unittest/)
> can be found in the test file `tests/restore_test.yaml`.

### Content-sync

If `content-sync` is enabled, the following flags must be enabled as well:

- `db`
- `db.backup`
- `db.contentsync`

> Respective [helm unittests](https://github.com/helm-unittest/helm-unittest/)
> can be found in the test files `tests/contentsync_author_test.yaml`,
> `tests/contentsync_public_test.yaml` & `tests/contentsync_shared_test.yaml`.

## Known issues

None (so far).

## Known unsupported combinations

None (so far).
