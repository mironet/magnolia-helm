# Magnolia CMS Helm Chart

Helm Chart for [Magnolia CMS](https://www.magnolia-cms.com/).

This chart deploys Magnolia CMS as an author and one or more publics instances as `StatefulSets`.

[[_TOC_]]

## Install

Add the public Helm repository:

```bash
helm repo add mironet https://charts.mirohost.ch
helm repo update
helm install -g mironet/magnolia-helm
```

This generates a deployment name (`-g`) and deploys the latest released version of the chart.

## Upgrade

The basic upgrade from an older chart version to a newer one is done with:

```bash
# Latest version:
helm upgrade <release-name> mironet/magnolia-helm
# Specific version:
helm upgrade <release-name> mironet/magnolia-helm --version <version-string>
```

Usually upgrades should work fine but there might be cases where you will have to redeploy some or all of the resources because of changes which are not allowed on existing Kubernetes objects. Since the databases and other stateful parts of the deployment use PVC templates it should be safe to simply delete the deployment:

```bash
helm delete <release-name> # This will not remove volumes created by PVC templates (inside StatefulSets).
helm install ... # And reinstall.
```

If you want to do this non-disruptively in production we recommend you restore/clone the deployment to another environment, point external load balancers to the new deployment and then remove the old one.

## Changelog

See [here](CHANGELOG.md).

## Values Reference

Setting the correct magnolia-helm values can be hard. This document
[here](tests/enabled-flags.md) tries to assist in setting the correct `enabled` flags.

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| bootstrap.enabled | bool | `true` | Do enable bootstrapping via REST. |
| deploy.directory | string | `"/usr/local/tomcat/webapps"` | Deploy into this directory innside the app server container. |
| deploy.extraLibs | string | `"/usr/local/tomcat/lib2"` |  |
| deploy.securityContext.fsGroup | int | `1000` | Fixup file permissions for volumes mounted to the Magnolia pod. |
| deploy.securityContext.runAsGroup | int | `1000` | Group ID. |
| deploy.securityContext.runAsUser | int | `1000` | Run application pod under this user ID. **Note:** Do not use a privileged user here. |
| deploy.tempDir | string | `"/usr/local/tomcat/temp"` |  |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.pullSecrets | list | `[]` |  |
| image.redirects.pullPolicy | string | `"IfNotPresent"` |  |
| image.redirects.repository | string | `"registry.gitlab.com/mironet/redirects"` |  |
| image.redirects.tag | string | `"v0.3-mainline"` |  |
| image.tomcat.pullPolicy | string | `"IfNotPresent"` | Tomcat repo pull policy. |
| image.tomcat.repository | string | `"tomcat"` | The tomcat image we're going to use. |
| image.tomcat.tag | string | `"9.0-jre17-temurin"` | Tomcat repo tag. |
| ingress.annotations | object | {} | Additional annotations for the ingress object. |
| ingress.enabled | bool | `false` | Enable/disable ingress configuration. |
| ingress.hosts | list | `[]` | Specify hosts here as an array. |
| ingress.tls | list | `[]` |  |
| jars[0].env[0].name | string | `"JAR_LIST_0"` |  |
| jars[0].env[0].value | string | `"postgresql-42.7.3.jar:/app/magnolia/WEB-INF/lib:postgresql.jar"` |  |
| jars[0].env[1].name | string | `"JAR_LIST_1"` |  |
| jars[0].env[1].value | string | `"jmx_prometheus_javaagent-1.0.1.jar:/extraLibs/:jmx_prometheus_javaagent.jar"` |  |
| jars[0].env[2].name | string | `"JAR_LIST_2"` |  |
| jars[0].env[2].value | string | `"jjwt-jackson-0.11.2.jar:/app/magnolia/WEB-INF/lib"` |  |
| jars[0].env[3].name | string | `"JAR_LIST_3"` |  |
| jars[0].env[3].value | string | `"jjwt-impl-0.11.2.jar:/app/magnolia/WEB-INF/lib"` |  |
| jars[0].env[4].name | string | `"JAR_LIST_4"` |  |
| jars[0].env[4].value | string | `"jjwt-api-0.11.2.jar:/app/magnolia/WEB-INF/lib"` |  |
| jars[0].env[5].name | string | `"JAR_LIST_5"` |  |
| jars[0].env[5].value | string | `"magnolia-rest-security-core-1.0.3.jar:/app/magnolia/WEB-INF/lib"` |  |
| jars[0].env[6].name | string | `"JAR_LIST_6"` |  |
| jars[0].env[6].value | string | `"magnolia-cloud-bootstrapper-1.0.14.jar:/app/magnolia/WEB-INF/lib"` |  |
| jars[0].initScript | string | `"/initGo.sh"` |  |
| jars[0].name | string | `"load-additional-jars"` |  |
| jars[0].repository | string | `"registry.gitlab.com/mironet/magnolia-jar/magnolia-jar-go"` |  |
| jars[0].tag | string | `"v0.1.1"` |  |
| linkerd.serviceInjection | object | `{"enabled":false}` | On cluster inject linkerd service mirror. |
| linkerd.serviceInjection.enabled | bool | `false` | Enable to inject Linkerd Service Mirror (requires Linkerd Installed and set up). |
| magnoliaAuthor | object | See values below ... | This is the author's configuration. It should not use H2 data base (the default). |
| magnoliaAuthor.activation.useExistingSecret | bool | `false` | Set this to `true` in case you want to use an existing activation key stored as a secret and provide its name. |
| magnoliaAuthor.base_url | string | `nil` | Pass URL to be set as Magnolia defaultBaseUrl. Setting is done on runtime via magnolia-bootrapper. |
| magnoliaAuthor.bootstrap.instructions | string | `""` | Verbatim content of the instructions for this instance. If empty use a default. This is intended to be used with the --set-file flag of "helm install". |
| magnoliaAuthor.catalinaExtraEnv | object | `{}` | These key/value pairs will be added to CATALINA_OPTS. |
| magnoliaAuthor.contextPath | string | `"/author"` | The context path of this Magnolia instance. Always use a leading slash. |
| magnoliaAuthor.db.backup.autoconfig.enabled | bool | `false` | Configures the backup for pg_wal automatically in a k8s environment. It does this by including a configmap via the envFrom: directive which will be injected into the pod. You can override or pass other environment variables via the env: or the extraEnv: directive below. |
| magnoliaAuthor.db.backup.enabled | bool | `false` | Enable db backup sidecar. |
| magnoliaAuthor.db.backup.volumeMounts | list | `[]` | Extra Volume Mounts Backup Sidecar |
| magnoliaAuthor.db.backup.volumes | list | `[]` | Extra Volume Backup Sidecar |
| magnoliaAuthor.db.contentsync.address | string | `":9998"` | TLS port of the backup sidecar. |
| magnoliaAuthor.db.jackrabbit.autoRepair | bool | `true` | Errors detected by a consistency check are automatically repaired. If false, errors are only written to the log. |
| magnoliaAuthor.db.jackrabbit.enableConsistencyCheck | bool | `false` | If set to true a consistency check is performed depending on the parameter forceConsistencyCheck. If set to false no consistency check is performed on startup, even if a redo log had been applied. |
| magnoliaAuthor.db.jackrabbit.extraSearchIndexParameters | object | `{}` | Extra search index parameters for jackrabbit configuration (e.g. overwrite search excerpt provider class with `excerptProviderClass`) |
| magnoliaAuthor.db.jackrabbit.forceConsistencyCheck | bool | `false` | Runs a consistency check on every startup. If false, a consistency check is only performed when the search index detects a prior forced shutdown. |
| magnoliaAuthor.db.jackrabbit.onWorkspaceInconsistency | string | `"log"` | If set to log, the process will just log the inconsistency during the re-indexing on startup. If set to fail, the process will fail the re-indexing on startup. |
| magnoliaAuthor.db.persistence.enabled | bool | `true` | If disabled, the database will not be persistent. Also contentsync and backup will no longer be supported in this case. Thus make sure to set .db.contentsync.enabled and .db.backup.enabled to false as well. |
| magnoliaAuthor.db.persistence.mountPath | string | `"/db"` | Mount point is /db, PGDATA=/db/data |
| magnoliaAuthor.db.persistence.subPath | string | `"data"` | Mount point is /db, PGDATA=/db/data |
| magnoliaAuthor.db.podAnnotations | object | `{}` | Custom annotations added to db pods. |
| magnoliaAuthor.db.restore.bundle_url | string | `"https://s3..."` | URL to backup bundle JSON file to use for restore. |
| magnoliaAuthor.db.restore.enabled | bool | `false` | Enable restore operations. |
| magnoliaAuthor.enabled | bool | `true` | If false, does not deploy the author instance. |
| magnoliaAuthor.extraContainers | list | `[]` | Extra sidecar containers added to the Magnolia pod. |
| magnoliaAuthor.extraInitContainers | list | `[]` | Extra init containers added to the Magnolia pod. |
| magnoliaAuthor.extraVolumeMounts | list | `[]` | Extra list of persistent volume mounts added to the Magnolia pod. |
| magnoliaAuthor.extraVolumes | list | `[]` | Extra list of persistent volume claims to be used by Magnolia or sidecars. |
| magnoliaAuthor.jndiResources | list | `[]` | Additional JDNI resources to be added in tomcat's `server.xml`. The key/value pairs will be mapped to xml. |
| magnoliaAuthor.persistence.enabled | bool | `true` | Enable persistence for indexes, cache, tmp files. If this is enabled the MGNL_HOME_DIR env var will be set and a volume will be mounted to the default location unless it's specified here as mountPath. |
| magnoliaAuthor.persistence.existingClaim | string | `nil` | Existing volumes can be mounted into the container. If not specified, helm will create a new PVC. |
| magnoliaAuthor.persistence.size | string | `"10Gi"` | In case of local-path provisioners this is not enforced. |
| magnoliaAuthor.persistence.storageClassName | string | `""` | Empty string means: Use the default storage class. |
| magnoliaAuthor.podAnnotations | object | `{}` | Custom annotations added to pod. |
| magnoliaAuthor.readinessProbe.initialDelaySeconds | int | `2` | Initial delay before first probe |
| magnoliaAuthor.readinessProbe.periodSeconds | int | `2` | Set probe interval |
| magnoliaAuthor.readinessProbe.successThreshold | int | `1` | Minimum consecutive successes for the probe to be considered successful after having failed |
| magnoliaAuthor.readinessProbe.timeoutSeconds | int | `1` | Number of seconds after which the probe times out |
| magnoliaAuthor.redeploy | bool | `false` | If true, redeploy on "helm upgrade/install" even if no changes were made. |
| magnoliaAuthor.rescueMode | bool | `false` | Enable Groovy rescue console. |
| magnoliaAuthor.resources.limits.memory | string | `"2Gi"` | Maximum amount of memory this pod is allowed to use. This is not the heap size, the heap size is smaller, see `setenv.memory` for details. |
| magnoliaAuthor.resources.requests.memory | string | `"2Gi"` | Minimum amount of memory this pod requests. |
| magnoliaAuthor.setenv.memory.maxPercentage | int | `60` | Maximum amount allocated to heap as a percentage of the pod's resources. |
| magnoliaAuthor.setenv.memory.minPercentage | int | `25` | Minimum amount allocated to heap as a percentage of the pod's resources. |
| magnoliaAuthor.setenv.update.auto | string | `"true"` | Auto-update Magnolia if repositories are empty (usually on the first run). |
| magnoliaAuthor.strategy.type | string | `"Recreate"` | Kubernetes rollout strategy on `helm upgrade ...`. |
| magnoliaAuthor.tomcat | object | `{"activateWebsockets":false,"customContext":{"attributes":{},"enabled":false,"withCookieProcessor":true},"error":{"showReport":false,"showServerInfo":false},"logArgs":false,"maxHttpHeaderSize":8192,"relaxedPathChars":null,"relaxedQueryChars":null,"remoteIpValve":{"attributes":{},"enabled":false}}` | Tomcat configuration |
| magnoliaAuthor.tomcat.activateWebsockets | bool | `false` | Activate the websockets |
| magnoliaAuthor.tomcat.customContext | object | `{"attributes":{},"enabled":false,"withCookieProcessor":true}` | Custom server context. |
| magnoliaAuthor.tomcat.customContext.attributes | object | `{}` | Custom attributes to be added to the context in the server.xml. |
| magnoliaAuthor.tomcat.customContext.enabled | bool | `false` | If enabled, the context in the server.xml will only have the custom attributes from the list below. |
| magnoliaAuthor.tomcat.customContext.withCookieProcessor | bool | `true` | If enabled, the context in the server.xml will have the nested "CookieProcessor" component. |
| magnoliaAuthor.tomcat.error | object | `{"showReport":false,"showServerInfo":false}` | Error valve configuration |
| magnoliaAuthor.tomcat.error.showReport | bool | `false` | Show error report |
| magnoliaAuthor.tomcat.error.showServerInfo | bool | `false` | Show server info |
| magnoliaAuthor.tomcat.logArgs | bool | `false` | Log JVM arguments |
| magnoliaAuthor.tomcat.maxHttpHeaderSize | int | `8192` | The maximum permitted size of the request line and headers associated with an HTTP request and response |
| magnoliaAuthor.tomcat.relaxedPathChars | string | `nil` | Allow additional special characters for path; allowed characters are: ``<>[\]^`{\|}`` |
| magnoliaAuthor.tomcat.relaxedQueryChars | string | `nil` | Allow additional special characters for query string; allowed characters are: ``<>[\]^`{\|}`` |
| magnoliaAuthor.tomcat.remoteIpValve.attributes | object | `{}` | Custom attributes to be added to the remote ip valve in the server.xml. |
| magnoliaAuthor.tomcat.remoteIpValve.enabled | bool | `false` | If enabled, a remote ip valve will be set in the server.xml. |
| magnoliaAuthor.webarchive.repository | string | `"registry.gitlab.com/mironet/magnolia-demo"` | The docker image where to fetch compiled Magnolia libs from. |
| magnoliaAuthor.webarchive.tag | string | `"latest"` | Do not use 'latest' in production. |
| magnoliaPublic | object | See values below ... | This is the public instance. |
| magnoliaPublic.activation.useExistingSecret | bool | `false` | Set this to `true` in case you want to use an existing activation key stored as a secret and provide its name. |
| magnoliaPublic.bootstrap.instructions | string | `""` | Verbatim content of the instructions for this instance. If empty use a default. This is intended to be used with the --set-file flag of "helm install". |
| magnoliaPublic.catalinaExtraEnv | object | `{}` | These key/value pairs will be added to CATALINA_OPTS. |
| magnoliaPublic.contextPath | string | `"/"` | The context path of this Magnolia instance. Always use a leading slash. |
| magnoliaPublic.db.backup.autoconfig.enabled | bool | `false` | Configures the backup for pg_wal automatically in a k8s environment. It does this by including a configmap via the envFrom: directive which will be injected into the pod. You can override or pass other environment variables via the env: or the extraEnv: directive below. |
| magnoliaPublic.db.backup.enabled | bool | `false` | Enable db backup sidecar. |
| magnoliaPublic.db.backup.volumeMounts | list | `[]` | Extra Volume Mounts Backup Sidecar |
| magnoliaPublic.db.backup.volumes | list | `[]` | Extra Volume Backup Sidecar |
| magnoliaPublic.db.contentsync.address | string | `":9998"` | TLS port of the backup sidecar. |
| magnoliaPublic.db.contentsync.enabled | bool | `false` | Enable content sync on public instances. Depends on the backup being enabled and configured correctly for pg_wal log shipping. |
| magnoliaPublic.db.enabled | bool | `true` | If disabled, the database will not be persistent. Also contentsync and backup will no longer be supported in this case. Thus make sure to set .db.contentsync.enabled and .db.backup.enabled to false as well. |
| magnoliaPublic.db.jackrabbit.autoRepair | bool | `true` | Errors detected by a consistency check are automatically repaired. If false, errors are only written to the log. |
| magnoliaPublic.db.jackrabbit.enableConsistencyCheck | bool | `false` | If set to true a consistency check is performed depending on the parameter forceConsistencyCheck. If set to false no consistency check is performed on startup, even if a redo log had been applied. |
| magnoliaPublic.db.jackrabbit.extraSearchIndexParameters | object | `{}` | Extra search index parameters for jackrabbit configuration (e.g. overwrite search excerpt provider class with `excerptProviderClass`) |
| magnoliaPublic.db.jackrabbit.forceConsistencyCheck | bool | `false` | Runs a consistency check on every startup. If false, a consistency check is only performed when the search index detects a prior forced shutdown. |
| magnoliaPublic.db.jackrabbit.onWorkspaceInconsistency | string | `"log"` | If set to log, the process will just log the inconsistency during the re-indexing on startup. If set to fail, the process will fail the re-indexing on startup. |
| magnoliaPublic.db.persistence.mountPath | string | `"/db"` | Mount point is /db, PGDATA=/db/data |
| magnoliaPublic.db.persistence.subPath | string | `"data"` | Mount point is /db, PGDATA=/db/data |
| magnoliaPublic.db.restore.bundle_url | string | `"https://s3..."` | URL to backup bundle JSON file to use for restore. |
| magnoliaPublic.db.restore.enabled | bool | `false` | Enable restore operations. |
| magnoliaPublic.extraContainers | list | `[]` | Extra sidecar containers added to the Magnolia pod. |
| magnoliaPublic.extraInitContainers | list | `[]` | Extra init containers added to the Magnolia pod. |
| magnoliaPublic.extraVolumeMounts | list | `[]` | Extra list of persistent volume mounts added to the Magnolia pod. |
| magnoliaPublic.extraVolumes | list | `[]` | Extra list of persistent volume claims to be used by Magnolia or sidecars. |
| magnoliaPublic.jndiResources | list | `[]` | Additional JDNI resources to be added in tomcat's `server.xml`. The key/value pairs will be mapped to xml. |
| magnoliaPublic.persistence.enabled | bool | `true` | Enable persistence for indexes, cache, tmp files. If this is enabled the MGNL_HOME_DIR env var will be set and a volume will be mounted to the default location unless it's specified here as mountPath. |
| magnoliaPublic.persistence.existingClaim | string | `nil` | Existing volumes can be mounted into the container. If not specified, helm will create a new PVC. |
| magnoliaPublic.persistence.size | string | `"10Gi"` | In case of local-path provisioners this is not enforced. |
| magnoliaPublic.persistence.storageClassName | string | `""` | Empty string means: Use the default storage class. |
| magnoliaPublic.podAnnotations | object | `{}` | Custom annotations added to pods. |
| magnoliaPublic.readinessProbe.initialDelaySeconds | int | `2` | Initial delay before first probe |
| magnoliaPublic.readinessProbe.periodSeconds | int | `2` | Set probe interval |
| magnoliaPublic.readinessProbe.successThreshold | int | `1` | Minimum consecutive successes for the probe to be considered successful after having failed |
| magnoliaPublic.readinessProbe.timeoutSeconds | int | `1` | Number of seconds after which the probe times out |
| magnoliaPublic.redeploy | bool | `true` | If true, redeploy on "helm upgrade/install" even if no changes were made. |
| magnoliaPublic.redirects.enabled | bool | `false` | Enable redirect reverse proxy. |
| magnoliaPublic.replicas | int | `1` | How many public instances to deploy. |
| magnoliaPublic.rescueMode | bool | `false` | Enable Groovy rescue console. |
| magnoliaPublic.resources.limits.memory | string | `"2Gi"` | Maximum amount of memory this pod is allowed to use. This is not the heap size, the heap size is smaller, see `setenv.memory` for details. |
| magnoliaPublic.resources.requests.memory | string | `"2Gi"` | Minimum amount of memory this pod requests. |
| magnoliaPublic.setenv.memory.maxPercentage | int | `60` | Maximum amount allocated to heap as a percentage of the pod's resources. |
| magnoliaPublic.setenv.memory.minPercentage | int | `25` | Minimum amount allocated to heap as a percentage of the pod's resources. |
| magnoliaPublic.setenv.update.auto | string | `"true"` | Auto-update Magnolia if repositories are empty (usually on the first run). |
| magnoliaPublic.strategy.type | string | `"Recreate"` | Kubernetes rollout strategy on `helm upgrade ...`. |
| magnoliaPublic.tomcat | object | `{"activateWebsockets":false,"customContext":{"attributes":{},"enabled":false,"withCookieProcessor":true},"error":{"showReport":false,"showServerInfo":false},"logArgs":false,"maxHttpHeaderSize":8192,"relaxedPathChars":null,"relaxedQueryChars":null,"remoteIpValve":{"attributes":{},"enabled":false}}` | Tomcat configuration |
| magnoliaPublic.tomcat.activateWebsockets | bool | `false` | Activate the websockets |
| magnoliaPublic.tomcat.customContext.attributes | object | `{}` | Custom attributes to be added to the context in the server.xml. |
| magnoliaPublic.tomcat.customContext.enabled | bool | `false` | If enabled, the context in the server.xml will only have the custom attributes from the list below. |
| magnoliaPublic.tomcat.customContext.withCookieProcessor | bool | `true` | If enabled, the context in the server.xml will have the nested "CookieProcessor" component. |
| magnoliaPublic.tomcat.error | object | `{"showReport":false,"showServerInfo":false}` | Error valve configuration |
| magnoliaPublic.tomcat.error.showReport | bool | `false` | Show error report |
| magnoliaPublic.tomcat.error.showServerInfo | bool | `false` | Show server info |
| magnoliaPublic.tomcat.logArgs | bool | `false` | Log JVM arguments |
| magnoliaPublic.tomcat.maxHttpHeaderSize | int | `8192` | The maximum permitted size of the request line and headers associated with an HTTP request and response |
| magnoliaPublic.tomcat.relaxedPathChars | string | `nil` | Allow additional special characters for path; allowed characters are: ``<>[\]^`{\|}`` |
| magnoliaPublic.tomcat.relaxedQueryChars | string | `nil` | Allow additional special characters for query string; allowed characters are: ``<>[\]^`{\|}`` |
| magnoliaPublic.tomcat.remoteIpValve.attributes | object | `{}` | Custom attributes to be added to the remote ip valve in the server.xml. |
| magnoliaPublic.tomcat.remoteIpValve.enabled | bool | `false` | If enabled, a remote ip valve will be set in the server.xml. |
| magnoliaPublic.webarchive.repository | string | `"registry.gitlab.com/mironet/magnolia-demo"` | The docker image where to fetch compiled Magnolia libs from. |
| magnoliaPublic.webarchive.tag | string | `"latest"` | Do not use 'latest' in production. |
| metrics.enabled | bool | `true` | Enable JMX exporters. |
| metrics.metricsServerPort | int | `8000` |  |
| metrics.setPrometheusAnnotations | bool | `true` |  |
| multicluster | object | `{"author":{"enabled":false,"remoteclusternames":[]}}` | Multicluster based on Linkerd. |
| multicluster.author.enabled | bool | `false` | Enable to define author on cluster. |
| multicluster.author.remoteclusternames | list | `[]` | Linked Remoteclusternames via Linkerd. |
| nameOverride | string | `""` |  |
| nodeAffinity | object | `{}` | Schedule Magnolia to labeled nodes, considering [nodeAffinity expressions](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#node-affinity) (may interfere with 'nodeSelector').   |
| nodeSelector | list | `[]` | Use Node Selector to schedule Magnolia Workload. |
| postjob.image | string | `"registry.gitlab.com/mironet/magnolia-bootstrap"` | Where to get the bootstrapper from. This should not be changed under normal circumstances. |
| postjob.imagePullPolicy | string | `"IfNotPresent"` |  |
| postjob.tag | string | `"v0.11-mainline"` |  |
| postjob.waitFor | string | `"10m"` |  |
| service.annotations | object | `{}` |  |
| service.clusterIP | string | `"None"` |  |
| service.ports[0].name | string | `"http"` |  |
| service.ports[0].port | int | `80` |  |
| service.ports[0].protocol | string | `"TCP"` |  |
| service.ports[0].targetPort | int | `8080` |  |
| service.ports[1].name | string | `"https"` |  |
| service.ports[1].port | int | `443` |  |
| service.ports[1].protocol | string | `"TCP"` |  |
| service.ports[1].targetPort | int | `8443` |  |
| service.redirectPort | int | `8081` |  |
| service.type | string | `"ClusterIP"` |  |
| sharedDb | object | See values below ... | Shared database (jackrabbit "clustering"). |
| sharedDb.db.contentsync.address | string | `":9998"` | TLS port of the backup sidecar. |
| sharedDb.db.jackrabbit.autoRepair | bool | `true` | Errors detected by a consistency check are automatically repaired. If false, errors are only written to the log. |
| sharedDb.db.jackrabbit.enableConsistencyCheck | bool | `false` | If set to true a consistency check is performed depending on the parameter forceConsistencyCheck. If set to false no consistency check is performed on startup, even if a redo log had been applied. |
| sharedDb.db.jackrabbit.extraSearchIndexParameters | object | `{}` | Extra search index parameters for jackrabbit configuration (e.g. overwrite search excerpt provider class with `excerptProviderClass`) |
| sharedDb.db.jackrabbit.forceConsistencyCheck | bool | `false` | Runs a consistency check on every startup. If false, a consistency check is only performed when the search index detects a prior forced shutdown. |
| sharedDb.db.jackrabbit.onWorkspaceInconsistency | string | `"log"` | If set to log, the process will just log the inconsistency during the re-indexing on startup. If set to fail, the process will fail the re-indexing on startup. |
| sharedDb.db.persistence.mountPath | string | `"/db"` | Mount point is /db, PGDATA=/db/data |
| sharedDb.db.persistence.subPath | string | `"data"` | Mount point is /db, PGDATA=/db/data |
| sharedDb.db.podAnnotations | object | `{}` | Custom annotations added to db pods. |
| sharedDb.db.restore.bundle_url | string | `"https://s3..."` | URL to backup bundle JSON file to use for restore. |
| sharedDb.db.restore.enabled | bool | `false` | Enable restore operations. |
| sharedDb.enabled | bool | `false` | Enable shared db |
| timezone | string | `"Europe/Zurich"` | Timezone for Magnolia. |
| tolerations | list | `[]` | Node Toleration for Magnolia Workload. |
| workspaces | list | `[]` | additional jcr workspaces that are NOT clustered. |

## Configuration

### Application Server

The tomcat setup is derived from the public tomcat helm chart and uses init
containers to copy the actual webapp to the `webapps` folder.

### Docker Image configuration

You can specify the tomcat image and tag in the values:

```yaml
image:
  tomcat:
    repository: tomcat
    tag: "9-jre11-slim"
  #...
```

The actual webapp is specified here:

```yaml
image:
  webarchive:
    repository: registry.gitlab.com/example/k8s/next-deployment
    tag: latest
  #...
```

The init container is expected to have an already "exploded" webapp in
`/magnolia`. Files in there will be copied into the `webapps` when starting
tomcat.

To pull from a private Docker registry (e.g. GitLab), you have to create a
docker-registry secret:

```bash
kubectl create secret docker-registry gitlab-registry --docker-server=https://registry.gitlab.com --docker-username=<username> --docker-password=<password or token>
```

To use the token from above, specify `pullSecrets` inside `image:` section like
the following:

```yaml
image:
  #...
  pullSecrets:
    - name: gitlab-registry
```

Or you can use service accounts, see [here](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/#add-image-pull-secret-to-service-account) for details.

### Persistence

In the `magnoliaPublic/Author:` sections of the values you can configure the
Magnolia instances (public and author). This is an example with PostgreSQL as a
backend data base and it's the ony db currently supported.

```yaml
magnoliaAuthor:
  db:
    enabled: true
    repository: postgres
    tag: 11.5-alpine
    type: postgres
    name: author
    persistence:
      enabled: true
      size: 10Gi
```

If you enable persistence a PVC is created and you can also specify the
StorageClass. Each instance (author and public) only gets one single db.

If you like to enable shared database (aka
[Jackrabbit Clustering](https://wiki.magnolia-cms.com/display/WIKI/Setting+up+a+Jackrabbit+Clustering)),
you can configure the shared workspaces and db connection (same as above) like
the following:

```yaml
sharedDb:
  enabled: false
  workspaces:
    - form2db
    - shop
  db: ...
```

### Libraries

If you need additional libraries (jars) you can specify them in the `jars:`
array. This is also how the PostgreSQL JDBC driver is being loaded.

```yaml
  - name: postgres-jdbc
    repository: registry.gitlab.com/mironet/magnolia-jar/postgres-42.2.8
    tag: v0.0.1
    env:
      - name: INIT_DEST
        value: /app/magnolia/WEB-INF/lib
    initScript: /init.sh
```

### Convention expected from init containers

This chart expects the init containers (see the `jars:` array above) to contain
an `/init.sh` script which is called as the only command. As of now the only
tasks expected from init containers is to copy some files to a target directory
specified by the env var `INIT_DEST`.

### Logging

To configure logging, you can use the following section:

```yaml
magnoliaAuthor:
  logging:
    level: DEBUG
    pattern: '{"level":"%p","timestamp":"%d{ISO8601}","file":"%c:%L","message":"%m"}%n'
    loggers:
      - name: my-logger
        level: ERROR
    appenders:
      - name: my-appender
        tagName: CustomTag
        properties:
          key1: value1
          key2: value2
          key3: value3
```

Under `loggers` it's possible to define additional loggers with the respective
value.

Under `appenders` it's possible to define additional appenders with the
respective tagName and properties.

> **Note:** Do not log to files inside the container. Always log to `stdout` or
`stderr` if possible or use a different log shipping mechanism.

### Rescue mode

To enable the
[Groovy Rescue Console](https://documentation.magnolia-cms.com/display/DOCS61/Groovy+module#Groovymodule-RescueApp),
deploy your Helm release with the following flag

for CE projects:

```yaml
magnoliaAuthor:
  rescueMode: true
```

for DX Core projects:

```yaml
magnoliaAuthor:
  rescueModeDX: true
```

This only takes effect if Magnolia is already installed.

## Monitoring / Liveness

As a default, we monitor the public instance via a call to the
[bootstrapper](https://gitlab.com/mironet/magnolia-bootstrap)'s `/readyz` and `/livez`
endpoints.

```yaml
magnoliaPublic:
  #...
  livenessProbe:
    port: 8765 # The default used by the bootstrapper.
    failureThreshold: 4
    initialDelaySeconds: 120
    timeoutSeconds: 10
    periodSeconds: 30
```

Magnolia is not deemed "ready" before the following conditions are met:

- Magnolia itself is reporting a good response to a healthcheck.

> **Note:** The bootstrapping is executed asynchronously after Magnolia itself
> reports healthy. This allows for a short period of a state during wich not all
> configuration has been applied but this has proven not to be an issue in
> production. Having a runtime dependency on it though has been annyoing, hence
> this is the default behaviour.

## TLS

When run in a Kubernetes environment and `cert-manager` is present you can
prepare an ingress object which should contain all annotations necessary for the
cert manager to issue certificates with eg. Let's Encrypt.

You have to first specify the host names you want certificates for and uncomment
the annotation used by cert-manager to auto-issue certificates from Let's
encrypt:

```yaml
ingress:
  enabled: True
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt-prod
  hosts:
    - host: test.k8s.example.com
      paths:
        - /
  tls:
    - hosts:
        - test.k8s.example.com
```

Of course you could still ignore all this and use your own load balancing /
ingress configuration, but this configuration is the one that has been tested.

## Extra sidecar/init containers

Additional sidecar and init containers can be injected in the
`magnoliaAuthor/magnoliaPublic:` sections in the `values.yml`:

```yaml
magnoliaPublic:
  #...
  extraContainers:
    - name: ohhai
      image: nginx:latest
      # ... further configuration according to the k8s containers section syntax.
  extraInitContainers:
    ... # Same syntax as k8s initContainers definition.
```

## Pod Annotations

You can add your own pod annotations for Magnolia deployments with the `podAnnotations` dictionary. Let's say you want to add Prometheus annotations for your pods running Magnolia:

```yaml
magnoliaPublic:
  podAnnotations:
    prometheus.io/scrape: "true"
    prometheus.io/port: "8080"
    prometheus.io/path: "/.monitoring/metrics"
```

## JNDI Resources

You can add additional JNDI resources (usually configured in Tomcat's `server.xml`) by adding the respective XML attribute keys and values. This definition for example ...

```yaml
magnoliaPublic:
  jndiResources:
    - name: "jdbc/custom-postgres"
      auth: "Container"
      type: "javax.sql.DataSource"
      driverClassName: "org.postgresql.Driver"
      url: "jdbc:postgresql://postgres.example.com:5432/database"
      username: "${jndi.postgres.username}"
      password: "${jndi.postgres.password}"
      maxTotal: "20"
      maxIdle: "10"
      maxWaitMillis: "-1"
```

... results in a `server.xml` config section like this:

```xml
<Server port="-1" shutdown="SHUTDOWN">
  <!-- Global JNDI resources
        Documentation at /docs/jndi-resources-howto.html
  -->
  <GlobalNamingResources>
    ...
    <Resource
              auth="Container"
              driverClassName="org.postgresql.Driver"
              maxIdle="10"
              maxTotal="20"
              maxWaitMillis="-1"
              name="jdbc/custom-postgres"
              password="${jndi.postgres.password}"
              type="javax.sql.DataSource"
              url="jdbc:postgresql://postgres.example.com:5432/database"
              username="${jndi.postgres.username}"
              />
  </GlobalNamingResources>
  ...
</Server>
```

> **Note:** Any combination of XML attribute names and values can be used here. They will be copied 1:1 into the target XML structure.

## Custom context configuration in `server.xml`

You can customize the context configuration in the `server.xml` file by using
the `.tomcat.customContext` block in your values. The values

```yaml
magnoliaAuthor:
  tomcat:
    customContext:
      enabled: True
      withCookieProcessor: False
      attributes:
        docBase: value1
        path: /
        key3: value3
        key4: value4
```

 for example would result in this context in the `server.xml`:

```xml
<Context docBase="value1" path="/" key3="value3" key4="value4" >
</Context>
```

> **Note:** The first two attributes `docBase` and `path` are mandatory.

You can use the `magnoliaPublic.tomcat.customContext` block to customize the
server context for the public instances too.

## Remote ip valve in `server.xml`

You can activate a remote ip valve and customize its configuration in the
`server.xml` file by using the `.tomcat.remoteIpValve` block in your values. The
values

```yaml
magnoliaAuthor:
  tomcat:
    remoteIpValve:
      enabled: True
      attributes:
        key1: value1
        key2: value2
```

for example would result in this context in the `server.xml`:

```xml
<Valve className="org.apache.catalina.valves.RemoteIpValve" key1="value1" key2="value2"  />
```

You can use the `magnoliaPublic.tomcat.remoteIpValve` block to customize the
server config for the public instances too.

## Microprofile config

This chart creates a helm-managed microprofile config map which is used by the
magnolia-bootstrap container to register public receivers on the author
instance. The helm chart user is very strongly advised against using this config
map for his/her own microprofile configuration. This is because the file content
does not persist between `helm upgrades` and is not intended to be used for user
configuration.

> ⚠️ CAUTION: Do **only** set extraEnv `smallrye.config.profile` in helm value
>`.catalinaExtraEnv` if you know what you are doing! Setting this incorrectly
> will break all helm-managed microprofile configuration. For author instances
> there must always be an `author` profile, for public instances there must always
> be a `public` profile set. Thus if you want to add a custom profile you must set
> the `author`/`public` profile **and** your custom profile (see
> [here](https://smallrye.io/smallrye-config/Main/config/profiles/#multiple-profiles)
> how).

### User-managed microprofile configuration

Provided that there exists a config map named `my-microprofile-configmap` with a
data key `microprofile-config.properties` and a data value which is the content
of your microprofile file, then you must only specify this extra volume and
volume mount in the `magnoliaAuthor`/`magnoliaPublic` block of your values file
to enable a user-managed microprofile config:

```yaml
  extraVolumes:
    - name: my-microprofile-config
      configMap:
        name: my-microprofile-configmap
        defaultMode: 0420
        optional: false
  extraVolumeMounts:
    - name: my-microprofile-config
      mountPath: /usr/local/tomcat/webapps/magnolia/WEB-INF/config/default/microprofile-config.properties
      subPath: microprofile-config.properties
```

> Note: The exact `mountPath` shown in the example above must be used to ensure
that Magnolia is considering your microprofile file.

## Expanding `$CATALINA_OPTS`

In the example about JNDI datasources we used variables which can be set by expanding `$CATALINA_OPTS`. This can be done in a declarative way in YAML:

```yaml
magnoliaPublic:
  env:
    - name: JNDI_POSTGRES_USERNAME
      valueFrom:
        secretKeyRef:
          key: postgres.username
          name: jndi
    - name: JNDI_POSTGRES_PASSSWORD
      valueFrom:
        secretKeyRef:
          key: postgres.password
          name: jndi
  catalinaExtraEnv:
    jndi.postgres.username: "${JNDI_POSTGRES_USERNAME}"
    jndi.postgres.password: "${JNDI_POSTGRES_PASSSWORD}"
```

This way we expect a previously existing secret with the username and password for the external database resource.

> **Note**: You could also use the variable name directly, e.g.
> `jndi.postgres.username` but the example here is about showing the declarative
> expansion of `$CATALINA_OPTS`.

## Backups / DB Dumps

The magnolia to object storage backup agent can be used for regular dumps and
backups of the data bases.

### Prerequisites

- S3 bucket with credentials (accesskey and secretkey) **or**
- GCS storage access (key.json file from the Google Console)
- Secret in Kubernetes

### Backup Configuration

You need to set at least the following values according to your environment.
Please have a look at the
[magnolia-backup documentation](https://gitlab.com/mironet/magnolia-backup) for
an explanation of all settings.

```yaml
magnoliaAuthor:
  db:
    backup:
      enabled: True
      env:
        - name: MGNLBACKUP_CMD
          value: pg_dumpall
        - name: MGNLBACKUP_ARGS
          value: --host localhost --user postgres
        - name: MGNLBACKUP_S3_BUCKET
          value: magnoliabackups
        - name: MGNLBACKUP_S3_ACCESSKEY
          valueFrom:
            secretKeyRef:
              name: s3-backup-key
              key: accesskey
        - name: MGNLBACKUP_S3_SECRETKEY
          valueFrom:
            secretKeyRef:
              name: s3-backup-key
              key: secretkey
        - name: MGNLBACKUP_S3_ENDPOINT
          value: s3.example.com
        - name: MGNLBACKUP_S3_CYCLE
          value: "15,4,3"
        - name: MGNLBACKUP_HERITAGE
          value: "my_backup_tag" # Backups will be marked with this tag in object storage.
```

The referenced secret needs to exist before rolling this out. Here's an example
of how to create one:

```bash
# Create files needed for the rest of the example.
echo -n 's3user' > accesskey.txt
 echo -n 'supersecrets3pass' > secretkey.txt

kubectl create secret generic s3-backup-key --from-file=accesskey=./accesskey.txt --from-file=secretkey=./secretkey.txt

rm -f accesskey.txt secretkey.txt
```

(Also see the
[official Kubernetes documentation about secrets](https://kubernetes.io/docs/concepts/configuration/secret/).)

This creates a new sidecar which takes backups every 24h each day. The backups
are streamed directly to S3 without temporarily storing them locally. Because a db dump has proven to be an expensive operation in our tests depending on the size of the database, this method is best suited for smaller (< 1 GiB) databases.

In case of larger data bases (a few GiBs to several TiBs) we recommend to use
[the WAL log shipping method](https://gitlab.com/mironet/magnolia-backup#postgresql-wal-archiving).

#### S3-Backup-Key Migration

A bug which has been fixed in [2f2f6c1f](https://gitlab.com/mironet/magnolia-helm/-/commit/bf6e4c3781be32779ab0df2898e4e58c72dc6a12), lead Magnolia Releases prior magnolia-helm `>=v1.5.15` to accidentially bundle a generic `<release>-s3-backup-key` to the helm deployment. This ended up in overwriting the secrets key-values on the next update, if certain `values.yaml` constellations are transmitted.

To migrate those secrets and dereference any Helm-Reference for the secrets, please use the Migrationscript provided in `/docs/migrateS3BackupKeys/migrateS3BackupSecrets.sh`:

```bash
# Ensure kubectl connection is set up to manage your Magnolia-Helm Deployment
# Target release by setting the $RELEASE env
export RELEASE="mynamespace"

#  Start Migration Script and follow instructions
cd docs/migrateS3BackupKeys
chmod +x migrateS3BackupSecrets.sh
./migrateS3BackupSecrets.sh
```

The script will recreate the `<release>-s3-backup-key` and backup the exisiting one to the local folder in case you need to recover.

### Backup inspection

You can port-forward the backup service and see a list of current backups and
also get direct download links for the S3 server:

```bash
kubectl port-forward <your-release-name>-author-db-0 9999:9999
```

After the port forwarding is established, visit
[http://localhost:9999/list](http://localhost:9999/list) for a list of backups.

## Content Sync

This chart supports spinning up new public instances by synchronizing the database before starting Magnolia resulting in a clone of other (available) public instances.

### Prerequisites

For this to work a few things need to be configured correctly:

- Backup with PG_WAL log shipping method (see [section before](#backups-db-dumps)).
- `cert-manager` needs to be present in the cluster to auto-issue certificates
  for mTLS.

> **Note:** We are working on eliminating the hard requirement for
> `cert-manager`. For now this is the only option.

### Mode of Operation

When spinning up new public databases by setting the `replica: n` value and if
`contentsync.enabled == true`, the new database will try to sync the content
from an already running public database. It will copy the whole database using a
_base backup_. This feature thus only works with PostgreSQL.

After starting the database it is safe to start a new public instance too by
setting `replica: n` of the public `StatefulSet`. It will match the ordinal
number automatically in its database configuration.

The content transfer itself is encrypted and the contentsync server and client
are authenticated by mTLS. There's currently no option to disable this (i.e.
"--insecure" or similar). TLS certificates, CA and client/server certificates,
are auto-generated by `cert-manager` when deployed with helm. `cert-manager` and
its CRDs need to be present in the cluster before using this feature (see
prerequisites above).

Also see the [magnolia-backup documentation](https://gitlab.com/mironet/magnolia-backup) for an explanation of
how this works.

## Activation keypair generation

### Technical background

Magnolia uses a public/private key system for content activation from author to
public instances. If that key is nonexistent on the first boot it will be
generated by Magnolia itself. While this is convenient it makes the application
somewhat stateful.

### Automatically generated key

This is the easiest approach. A `emptyDir` will be created by the pod and
mounted for the purpose of storing the automatically generated activation key.
You don't have to configure anything for this and it is the recommended approach
(the default). Keys will be autogenerated if missing (for example if you delete
the pod and the emptydir volume) and distributed to public instances
automatically.

### Secrets

You can specify which secret the pods should be looking for to mount the key
`.properties` file at the correct location. The format of the activation key
file is like follows:

```text
#generated 09.May.2020 04:55 by superuser
#Sat May 09 16:55:03 CEST 2020
key.private=30820277020100300...
key.public=30819F300D06092A8648...
```

### Regenerating the key pair

If you [regenerate keys in admin
central](https://docs.magnolia-cms.com/product-docs/6.2/Administration/Security/Activation-security/Activation-keys.html#_regenerating_the_key_pair),
the public keys *will be distributed to the public instance automatically* with
the help of the `magnolia-bootstrap` sidecar. There's no need to manually copy
them to the public instances anymore (contrary to Magnolia's documentation). You will see logs similar to these on the public bootstrapper sidecars:

```text
time="2023-09-12T17:00:21Z" level=info msg="activation secret activation-key: received event type MODIFIED"
time="2023-09-12T17:00:23Z" level=info msg="👍 success: setting public key"
```

and these on author instances:

```text
time="2023-09-12T17:00:21Z" level=info msg="activation secret activation-key: received event type MODIFIED"
time="2023-09-12T17:00:21Z" level=info msg="👍 success: writing activation key to secret"
```

### Direct key input

> **Note:** The following procedure is usually not needed, nor recommended
(because it encourages key re-usage). Use the automated method above, which is
the default.

> ⚠ If you've been using the direct key input method, it is safe to
switch to the automated (default) method now.

Magnolia requires RSA keypair properties for the publication mechanism. You can
generate a keypair e.g. with `openssl`.

> **Note:** Magnolia up to version 6.2.33 can handle at most 1024 bit key length.

```bash
mkdir temp
openssl genrsa -out temp/key.pem 4096
openssl rsa -in temp/key.pem -pubout -outform DER -out temp/pubkey.der
openssl pkcs8 -topk8 -in temp/key.pem -nocrypt -outform DER -out temp/key.der
```

Now you can create the `activation.properties` secret:

```bash
echo key.public=$(xxd -p temp/pubkey.der | tr -d '\n') > temp/secret.yml
echo key.private=$(xxd -p temp/key.der | tr -d '\n') >> temp/secret.yml
kubectl create secret generic activation-key --from-file=activation-secret=temp/secret.yml
```

The configuration in your `values.yml` should reference this secret so it will
be loaded:

```yaml
bootstrap:
  enabled: True

magnoliaAuthor:
  activation:
    useExistingSecret: True
    secret:
      name: activation-key
      key: activation-secret

magnoliaPublic:
  activation:
    useExistingSecret: True
    secret:
      name: activation-key
      key: activation-secret
```

> **Note:** If the key size does not align with the current Magnolia version in
use, it will undergo automatic regeneration, leading to the overwriting of the
secret in Kubernetes during runtime.

## Redirects servers in public instances

Starting from `v1.6.0` redirects server responding with redirects defined in a
config map can be activated for all public Magnolia instances. Requests not
found in the redirect list are proxied to the redirects server's respective
public magnolia instance.

To activate the feature set helm value `magnoliaPublic.redirects.enabled` to
`True`.

The redirects are defined in a config map which must be named
`<release-name>-magnolia-redirects` and be located in the namespace of the
release. It is used for all public instances at the same time, i.e. all public
instances are automatically configured to respond with the same redirects.

If there exists no redirects map for a release yet, an empty redirects config
map will be created by helm.

The redirects config map must follow the following structure:

```yaml
apiVersion: v1
data:
  rules.csv.gz: |-
    Source,Target,Code
    <1 to n lines of redirect rules>
kind: ConfigMap
metadata:
  name: <release-name>-magnolia-redirects
  namespace: <release-namespace>
```

Refer to the [redirects server
readme](https://gitlab.com/mironet/redirects#regex) to see how the redirect
rules must be formatted.

## Multicluster

To interconnect two or more Magnolia deployments on different clusters, one can use the `multicluster` feature of the K8s Service Mesh Project [Linkerd](https://linkerd.io/).

To be more precise, we'll use the [multicluster linking](https://linkerd.io/2.13/features/multicluster/) to connect the clusters and use the `headless service mirroring` of Linkerd to replicate the service state of remote Public instances to the primary cluster.
The `magnolia-bootstrapper` will automatically de-/register the remote Public Instances to the Author.

> **Note:** This setup is designed for **one single** Magnolia Author instance
in one cluster and one or more Magnolia Public instances in the same cluster
and/or in remote clusters. Multiple Magnolia Author instances are **not
supported**.

[Install and set up linkerd](https://linkerd.io/2.13/tasks/multicluster/#install-linkerd) accordingly in all participating clusters.
Once done, link the clusters for [`Multi-cluster communication with StatefulSets`](https://linkerd.io/2.13/tasks/multicluster-using-statefulsets/), which will enable the required `headless support` for service
mirroring.

To configure the `headless service mirroring`, set the enabled value to true as
in the example below. This should be done on all the sides (clusters), Author
and Public.

```yaml
# All magnolia deployments get this 👇
linkerd:
  serviceInjection:
    enabled: true
```

For the **Magnolia Author** side, use these values:

```yaml
# See above 👆
linkerd:
  serviceInjection:
    enabled: true

# This is specific for author 👇
multicluster:
  author:
    enabled: true
    remoteclusternames:
    - satellite01  # Example cluster name
```

> **Note:** The above enables only linkerd service mirrors!

To get intercluster communication working on an **existing deployment**, we need
to inject linkerd sidecars by annotating the namespace ...

```bash
# Inject Linkerd sidecar in all pods inside the namespace, in this example the `multiregion` namespace.
$ kubectl annotate namespaces multiregion "linkerd.io/inject"="enabled"
```

... and redeploy the `StatefulSet` or `Deployments`:

```bash
kubectl rollout restart deployment/statefulset <deployment/statefulset-name>
```

so Linkerd can inject the sidecars.

This helm chart can not do this automatically because it does not manage
namespaces or linkerd itself.

## Repull `magnolia-backup` and/or `magnolia-bootstrap` 0.X-mainline images

If you want to repull an image (e.g. `magnolia-backup` and/or
`magnolia-bootstrap`) on all nodes of your cluster, you can apply the following
DaemonSet:

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: repull-<image-name>
  namespace: <any-namespace>
spec:
  selector:
    matchLabels:
      magnolia.info: my-image-puller
  template:
    metadata:
      labels:
        magnolia.info: my-image-puller
      namespace: <any-namespace>
    spec:
      containers:
      - image: gcr.io/google-containers/pause:2.0
        imagePullPolicy: Always
        name: always-ready
        securityContext:
          allowPrivilegeEscalation: false
          privileged: false
          readOnlyRootFilesystem: false
          runAsNonRoot: false
      initContainers:
      - args:
        - --version
        command:
        - /app
        image: <image-repo>/<image-name>:<image-tag>
        imagePullPolicy: Always
        name: repull-<image-name>
        securityContext:
          allowPrivilegeEscalation: false
          privileged: false
          readOnlyRootFilesystem: false
          runAsNonRoot: false
      restartPolicy: Always
```

> Note: Replace `<any-namespace>`, `<image-repo>`, `<image-name>` and
> `<image-tag>` with the respective values.

Once the DaemonSet is applied and its status field `numberReady` is equal to the
number of nodes in the cluster, the DaemonSet can (and should) be deleted.

> Note: The DaemonSet should be deleted as otherwise it continues to run and
consumes resources for no reason.

### Use case / Example

Say you are using `magnolia-bootstrap@v0.8-mainline` in your magnolia-helm chart. At
the moment you installed or last updated your magnolia-helm deployment, the
`magnolia-bootstrap@v0.8-mainline` image was equivalent to the image
`magnolia-bootstrap@v0.8.0`.

> Note: At any point in time the `magnolia-bootstrap@v0.X-mainline` image is
> equivalent to the latest released bugfix for minor version `v0.X`.

Say a bug was discovered in `magnolia-bootstrap@v0.8.0` and a new version
`v0.8.1` was released that fixed this bug. After the release of that bugfix the
image `magnolia-bootstrap@v0.8-mainline` was usually also updated to be
equivalent to `magnolia-bootstrap@v0.8.1`.

To make use of the bugfix without having to update your magnolia-helm
deployment, you can use the above DaemonSet to repull the image
`magnolia-bootstrap@v0.8-mainline` on all nodes of your cluster.

Then by the next restart of your magnolia-bootstrap containers, the new image,
which contains the bugfix, will be used.

## Status

This chart is currently used in production. We will adhere to the semver
standard and try to maintain backwards compatiblity within major releases.

## Compatibility

Values used with older chart versions should always work with newer chart
versions and provide the same results.

> **Note:** This does not mean a certain deployment will upgrade non-disruptively, i.e. without having to remove it first. See the [Upgrade](#upgrade) section about upgrades in general.
>
> **Note**: Ingress API `extensions/v1beta1` [deprecated for Kuberenetes `v1.22` and above](https://kubernetes.io/blog/2021/07/14/upcoming-changes-in-kubernetes-1-22/#api-changes) and has been replaced by `networking.k8s.io/v1`. If you encounter any issues like:
>
>```text
> Error: UPGRADE FAILED: unable to recognize "": no matches for kind "Ingress" in version "networking.k8s.io/v1"
>```
>
> Please ensure to use `magnolia-helm v1.5.1` with [`Kubernetes v1.19` or higher](https://kubernetes.io/blog/2021/07/14/upcoming-changes-in-kubernetes-1-22/#api-changes), where using `networking.k8s.io/v1` was introduced.
>
> Also ensure using a [proper ingress port name value](https://kubernetes.io/docs/reference/kubernetes-api/service-resources/ingress-v1/#IngressBackend) (_a string, not a number!_) in `service.ports[0].name` rather than a `service.ports[0].number`.
> As they are mutually exlusive, only using a `service.ports[0].name` will be supported in future releases

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| MiroNet AG | <mathias.seiler@mironet.ch> | <https://www.mironet.ch/> |
| fastforward websolutions | <pzingg@fastforward.ch> | <https://www.fastforward.ch/> |

## Legal Notes

Magnolia, Magnolia Blossom, Magnolia and the Magnolia logo are registered
trademark or trademarks of Magnolia International Ltd.
